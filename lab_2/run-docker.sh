#!/bin/bash
docker build -t ctz:lab_2 -f df.lab_2 ~/geekbrains-conteinerization/homework/2.docker/python/ && \
docker run -d --rm -p 8080:8080 --name lab_2 ctz:lab_2
sleep 10
curl localhost:8080
docker stop lab_2
